package resources;

import aux.HttpResponse;
import entity.*;
import services.Context;
import services.RegistrationService;
import database.UserDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */

public class UserResource {

    Context context;
    UserDao userDao = new UserDao();
    RegistrationService registrationService = new RegistrationService();


    public UserResource(Context context) {
        this.context = context;
    }


    private boolean currentCondoIn(List<Integer> condos) {
        return condos
                .stream()
                .anyMatch((condo) -> condo == context.getCurrentCondo().getId());
    }

    private boolean isAdmin() {
        return context
                .getCurrentUser()
                .roles
                .stream()
                .anyMatch((Role r) ->
                        (r.roleId.equals(RoleId.SUPER)) || ((r.roleId.equals(RoleId.BOARD) || r.roleId.equals(RoleId.TRUSTEE)) && currentCondoIn(r.condos)));

    }

    private void validateAdminPermission() {
        if (!isAdmin()) {
            throw new PermissionDeniedException("Permission denied: not admin!!!");
        }else{
            System.out.println("Is admin");
        }
    }

    //user/{id}
    //post (create)
    public HttpResponse<User> createNewUser(UserDto dto) {
        try {
            validateAdminPermission();
        } catch (PermissionDeniedException e) {
            return new HttpResponse<>().setError(e.getMessage());
        }

        User user = new User();
        user.name = dto.name;
        user.roles = dto.roles;
        user.address = dto.address;
        user.hashPassword = registrationService.generateTempPassword(user);
        userDao.saveUser(user);
        registrationService.sendResistrationEmail(user, user.hashPassword);
        return new HttpResponse<>(user);
    }

    //put (update)
    public HttpResponse<User> updateUser(UserDto dto) {
        try {
            validateAdminPermission();
        } catch (PermissionDeniedException e) {
            return new HttpResponse<>().setError(e.getMessage());
        }
        User user = new User();
        user.address = dto.address;
        user.name = dto.name;
        user.roles = dto.roles;
        userDao.saveUser(user);
        return new HttpResponse<>(user);
    }

    //delete
    public HttpResponse<User> adminDeleteUser(int id) {
        try {
            validateAdminPermission();
        } catch (PermissionDeniedException e) {
            return new HttpResponse<>().setError(e.getMessage());
        }

        User user = userDao.delete(id);
        return new HttpResponse<>(user);
    }



}
