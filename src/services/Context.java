package services;

import database.CondoDao;
import database.UserDao;
import entity.Condo;
import entity.User;

/**
 * Created by bruno on 16/03/17.
 */
public class Context {
    UserDao u = new UserDao();
    CondoDao c = new CondoDao();
    User currentUser;

    static Context  contextInstance;

    private Context(){

    }
    public static synchronized Context getInstance(){
        if(contextInstance == null){
            contextInstance = new Context();
        }
        return contextInstance;
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public Condo getCurrentCondo(){
        return c.findAll().get(0);
    }
}
