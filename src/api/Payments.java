package api;

import entity.User;

/**
 * Created by bruno on 16/03/17.
 */
public interface Payments {
    boolean isOverdue(User user);
}
