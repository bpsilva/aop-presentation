package resources_aop;

import services.Context;
import entity.Role;
import entity.RoleId;
import entity.User;
import org.junit.Test;


import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by bruno on 16/03/17.
 */
public class Tests {
    Context context = Context.getInstance();
    UserResource userResource = new UserResource(context);

    @Test
    public void editUserAsAdminWithNoPermission(){
        context.setCurrentUser(new User());
        assertThat(userResource.adminDeleteUser(1).getError(), is("Permission denied: not admin!!!")) ;
    }

    @Test
    public void editUserAsAdminWithSuper(){
        User user = new User();
        user.roles = Arrays.asList(new Role(RoleId.SUPER, Arrays.asList()));
        context.setCurrentUser(user);

        assertThat(userResource.adminDeleteUser(1).getEntity().name, is("Deleted User"));
    }

    @Test
    public void editUserAsAdminTrusteeCondo(){
        User user = new User();
        user.roles = Arrays.asList(new Role(RoleId.TRUSTEE, Arrays.asList(context.getCurrentCondo().getId())));
        context.setCurrentUser(user);


        assertThat(userResource.adminDeleteUser(1).getEntity().name, is("Deleted User"));
    }


}
