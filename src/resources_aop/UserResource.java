package resources_aop;

import aux.HttpResponse;
import aux.Treat;
import services.Context;
import services.RegistrationService;
import database.UserDao;
import entity.User;
import entity.UserDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class UserResource {

    Context context;
    UserDao userDao = new UserDao();
    RegistrationService registrationService = new RegistrationService();


    public UserResource(Context context){
        this.context = context;
    }


    //user/{id}
    //post (create)
    public HttpResponse<User> adminCreateNewUser(UserDto dto) {

        System.out.println("Creating user");
        User user = new User();
        user.name = dto.name;
        user.roles = dto.roles;
        user.address = dto.address;
        user.hashPassword = registrationService.generateTempPassword(user);
        userDao.saveUser(user);
        registrationService.sendResistrationEmail(user, user.hashPassword);
        return new HttpResponse<>(user);
    }

    //put (update)
    public HttpResponse<User> adminUpdateUser(UserDto dto) {

        System.out.println("Updating user");
        User user = new User();
        user.address = dto.address;
        user.name = dto.name;
        user.roles = dto.roles;
        userDao.saveUser(user);
        return new HttpResponse<>(user);
    }

    //delete
    @Treat
    public HttpResponse<User> adminDeleteUser(int id) {

        System.out.println("Deleting user");
        User deletedUser = userDao.delete(id);
        return new HttpResponse<>(deletedUser);
    }




}
