package resources_aop;

import entity.PermissionDeniedException;
import services.Context;
import entity.Role;
import entity.RoleId;

import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public aspect ValidateAdminPermissionAdvice {
    Context context = Context.getInstance();

    pointcut adminRequired(): execution (* UserResource.admin*(..));

    before(): adminRequired() {
        System.out.println("Validating admin permission");
        if (!isAdmin()) {
            throw new PermissionDeniedException("Permission denied: not admin!!!");
        }
    }

    private boolean currentCondoIn(List<Integer> condos) {
        return condos
                .stream()
                .anyMatch((condo) -> condo == context.getCurrentCondo().getId());
    }

    private boolean isAdmin() {
        return context
                .getCurrentUser()
                .roles
                .stream()
                .anyMatch((Role r) ->
                        (r.roleId.equals(RoleId.SUPER)) || ((r.roleId.equals(RoleId.BOARD) || r.roleId.equals(RoleId.TRUSTEE)) && currentCondoIn(r.condos)));

    }

}
