package demonstration;

/**
 * Created by bruno on 16/03/17.
 */
public aspect World {

    pointcut greeting(): execution (void Hello.sayHello(..));

    after() returning(): greeting() {
        System.out.println(", world!!");
    }
}