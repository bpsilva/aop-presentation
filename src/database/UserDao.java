package database;

import entity.Condo;
import entity.Role;
import entity.RoleId;
import entity.User;

import java.util.Arrays;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class UserDao {
    List<User> users;
    protected Condo condoA = new Condo();
    protected Condo condoB = new Condo();
    private List<Integer> oneTwo = Arrays.asList(1, 2);
    private List<Integer> one = Arrays.asList(1);
    private List<Integer> two = Arrays.asList(2);

    public UserDao() {

        User a = new User(8, "Keanu Reeves", Arrays.asList(new Role(RoleId.TENANT, oneTwo),
                new Role(RoleId.OWNER, oneTwo),
                new Role(RoleId.BOARD, oneTwo),
                new Role(RoleId.TRUSTEE, oneTwo),

                new Role(RoleId.SUPER, oneTwo)));

        User b = new User(7, "Laurence Fishburne",
                Arrays.asList(new Role(RoleId.OWNER, oneTwo),
                        new Role(RoleId.BOARD, oneTwo),
                        new Role(RoleId.TRUSTEE, oneTwo),
                        new Role(RoleId.SUPER, oneTwo)));

        User c = new User(6, "Carrie-Anne Moss",
                Arrays.asList(new Role(RoleId.TENANT, one),
                        new Role(RoleId.OWNER, one),
                        new Role(RoleId.BOARD, one),
                        new Role(RoleId.TRUSTEE, one)));

        User d = new User(5, "Joe Pantoliano",
                Arrays.asList(new Role(RoleId.TENANT, two),
                        new Role(RoleId.OWNER, two),
                        new Role(RoleId.BOARD, two),
                        new Role(RoleId.TRUSTEE, two)));
        User e = new User(4, "Hugo Weaving",
                Arrays.asList(new Role(RoleId.TENANT, one),
                        new Role(RoleId.OWNER, one),
                        new Role(RoleId.BOARD, one)));

        User f = new User(3, "Lana Wachowski",
                Arrays.asList(new Role(RoleId.TENANT, one),
                        new Role(RoleId.OWNER, one),
                        new Role(RoleId.BOARD, one)));


        User g = new User(2, "Lilly Wachowski",
                Arrays.asList(new Role(RoleId.TENANT, two),
                        new Role(RoleId.OWNER, one)));

        users = Arrays.asList(a, b, c, d, e, f, g);
        condoA.users = Arrays.asList(a, c, e, g);
        condoB.users = Arrays.asList(b, d, f);
    }

    public User findById(int id) {
        return users
                .stream()
                .filter((u) -> u.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<User> findAll() {
        return users;
    }

    public List<User> findByCondo(int condoId) {
        if (condoId == 1) {
            return condoA.users;
        }
        return condoB.users;
    }

    public void saveUser(User user) {
        System.out.println("Saving user: " + user.name);
    }

    public User delete(int id){
        User u = new User();
        u.name = "Deleted User";
        return u;
    }
}
