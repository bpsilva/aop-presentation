package database;

import entity.Condo;

import java.util.Arrays;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class CondoDao {

    UserDao userDao = new UserDao();


    public List<Condo> findAll() {
        return Arrays.asList(userDao.condoA, userDao.condoB);
    }

    public Condo findById(int condoId) {
        if(condoId == 1){
            return userDao.condoA;
        }
        return userDao.condoB;
    }
}
