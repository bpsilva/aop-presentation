package entity;

import aux.Id;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class User {
    @Id
    private int id;

    public String name;
    public List<Role> roles;
    public String address;
    public String hashPassword;

    public User(){
        roles = new ArrayList<>();
    }

    public User(int id, String name, List<Role> roles){
        this.id = id;
        this.name = name;
        this.roles = roles;
        this.address = "apartment " + id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
