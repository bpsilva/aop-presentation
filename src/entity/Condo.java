package entity;

import aux.Id;

import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class Condo {

    @Id
    int id;

    public List<User> users;
    public String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
