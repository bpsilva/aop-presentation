package entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class Role {
    public RoleId roleId;
    public List<Integer> condos = new ArrayList<>();
    public Role(RoleId roleId, List<Integer> condos){
        this.condos = condos;
        this.roleId = roleId;
    }
}
