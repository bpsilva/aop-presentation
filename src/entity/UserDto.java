package entity;

import java.util.List;

/**
 * Created by bruno on 16/03/17.
 */
public class UserDto {
    public String name;
    public List<Role> roles;
    public String address;
    public String hashPassword;
    public String hashPasswordConfirm;
}
