package entity;

/**
 * Created by bruno on 16/03/17.
 */
public enum RoleId {
    TENANT, OWNER, BOARD, TRUSTEE, SUPER
}
